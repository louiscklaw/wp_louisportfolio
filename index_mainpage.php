  <head>
    <?php
      ob_start('sanitize_output');

      include $GLOBALS['DIR_INC'].'/meta.php';
      include $GLOBALS['DIR_INC'].'/favicon.php';
      include $GLOBALS['DIR_INC'].'/head.php';

      //test multilingual support
      // $var = 'apple';
      // $temp =get_query_var( $var, $default ) ;
      // fHTML_varexport($_GET);
    ?>
  </head>
  <body id="page-top" class="index">
  <?php
    include_once $GLOBALS['DIR_INC'].'/google_analytics.php';

    include $GLOBALS['DIR_INC'].'/nav.php';

    // get_header();
    include $GLOBALS['DIR_INC'].'/header.php';

    include $GLOBALS['DIR_INC'].'/section.php';

    // get_header();
    include $GLOBALS['DIR_INC'].'/footer.php';

    // for portfolio modals.
    include $GLOBALS['DIR_INC'].'/portfolio.php';

    echo '<!--timeline_models-->';
    echo $GLOBALS['timeline_models'];
    echo '<!--timeline_models_end-->';

    include $GLOBALS['DIR_INC'].'/load_js.php';

    // NOTE: add trianglify support
    include $GLOBALS['DIR_INC'].'/load_trianglify.php';

    include_once $GLOBALS['DIR_INC'].'/typekit.php';

    if ($_SERVER['ServerType'] == 'debug') {
        fHTML_varexport($_SERVER);
    }
     ?>
  </body>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
