<?php
  // NOTE: To test -> phpunit UnitTest test_submit_guestemail.php
  // require_once 'PHPUnit/Autoload.php';
  require_once '../submit_guestemail.php';

  class UserTest extends PHPUnit_Framework_TestCase
  {
      public $sTestEmail = 'test1@123.com';
      public $iDefaultCount = 10;

      protected function setUp()
      {
          // NOTE: configuration
          $username = 'littleredflower';
          $password = 'littleredflower';
          $database = 'littleredflower';
          $host = 'localhost';
          $TBL_USER_GUEST_TOKEN = 'user_guest_token';

          $sTestEmail = '123@123.com';

          $db = new MysqliDb($host, $username, $password, $database);
      }

      public function fClearTestEntry($sInputEmail)
      {
          // global $db;
          global $TBL_USER_GUEST_TOKEN;

          $db = MysqliDb::getInstance();
          print_r("\r\nfClearTestEntry->start");
          $db->where('email', $sInputEmail);
          if ($db->delete($TBL_USER_GUEST_TOKEN)) {
              print_r('->cleared');
          }
          print_r('->end');
          print_r("\r\n");
      }

      public function fAddTestAccount($sEmail)
      {
          fAddGuestEmail($sEmail, md5($sEmail), '10');
      }

      public function fGetAllRows()
      {
          global $TBL_USER_GUEST_TOKEN;
          $db = MysqliDb::getInstance();
          $Guests = $db->get($TBL_USER_GUEST_TOKEN);

          return $Guests;
      }

      public function fLookUpRow($sTable, $sFieldInput, $sFieldValue, $sLookupField)
      {
          $sOutput = '';
          try {
              $db = MysqliDb::getInstance();
              $db->where($sFieldInput, $sFieldValue);
              // print_r($sFieldInput);
              // print_r($sFieldValue);
              // print_r($db->count);
              $sOutput = $db->getOne($sTable)[$sLookupField];
          } catch (Exception $e) {
              print_r($e->getMessage());
          }

          return $sOutput;
      }

      public function testAddingEmail()
      {
          $bResult = false;
          // NOTE: clear table before Test1
          print_r("\r\nTest1 start");
          print_r("\r\nCreate db connection");

          $this->fClearTestEntry($this->sTestEmail);
          $this->fAddTestAccount($this->sTestEmail);

          $Guests = $this->fGetAllRows();
          foreach ($Guests as $idx => $Guest) {
              // var_export($Guest);
            if ($Guest['email'] == $this->sTestEmail) {
                $this->assertTrue(true);
            }
          }
      }

      public function testfDeductCountByGuestCode()
      {
          // NOTE: input = email
          // NOTE: get token from email
          $sToken = $this->fLookUpRow('user_guest_token', 'email', $this->sTestEmail, 'guestcode');

          // NOTE: try deduct iCountdown
          // NOTE: default 10 count
          // NOTE: check iCountdown is dedcuted
          for ($i = 1;$i <= $this->iDefaultCount; ++$i) {
              $bExam = fDeductCountByGuestCode($sToken);
              if ($i > $this->iDefaultCount) {
                  // NOTE: check function reutrn false (> 10)
                  $this->assertFalse($bExam);
              } else {
                  // NOTE: check function return true (< 10)
                  $this->assertTrue($bExam);
              }
          }
      }
  }
