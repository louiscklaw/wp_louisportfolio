<?php

    if (class_exists('cls_section')) {
    } else {
        include $GLOBALS['DIR_INC'].'/cls_section.php';
    }

    if (true) {
        echo "<!--portfolio.php-->\r\n";
    }

    // NOTE: input

    $wp_mainposts = get_posts(array(
      'post_parent' => 0,
      'post_type' => 'onepagepost',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'posts_per_page' => 99,
    ));

    $wp_mainpost = $wp_mainposts[0];
    $post_section_posts = get_posts(array(
        'post_parent' => $wp_mainpost->ID,
        'post_type' => 'onepagepost',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => 99,
    ));

    if (false) {
        fHTML_varexport($post_section_posts);
        die();
    }

    // NOTE: process
    $sTemp = '';
    $iCounter = 0;
    foreach ($post_section_posts as $post_section_post) {
        $post_custom_para = get_post_custom($post_section_post->ID);

        if ($post_custom_para['section_theme'][0] == 'fSection_Portfolio_Generate') {
            if (false) {
                echo fHTML_varexport($post_section_post->post_title);
                echo fHTML_varexport($post_custom_para);
                die();
            }

            $cSection = new cls_section();
            $sTemp = $sTemp.$cSection->fSection_Portfolio_Modals_Generate($post_section_post->ID);
        }
    }

    // NOTE: output
    echo $sTemp;

    if (true) {
        echo "<!--portfolio.php end-->\r\n";
    }
