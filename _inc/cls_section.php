<?php
    include dirname(__FILE__).'/../config.php';
    include dirname(__FILE__).'/cust_functions.php';

    $sCONTENT_SEPERATOR = '^,';

    function fGetTargetKeyFromArray($sTargetKey, $akInput)
    {
        $iOutput = -1;
        foreach ($akInput as $idx => $kInput) {
            if ($akInput[$idx]['key'] == $sTargetKey) {
                $iOutput = $idx;
            }
        }

        return $iOutput;
    }

    function fGetBasicPostContent($wp_args)
    {
        $aOutput = array();
        $wp_post = get_posts($wp_args);
        array_push($aOutput, array(
            'key' => 'POST_TITLE',
            'value' => $wp_post->post_title,
        ),
        array(
            'key' => 'FEATUREIMAGE',
            'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_post->ID), 'single-post-thumbnail')[0],
        )
        );

        return $aOutput;
    }

    function fGetPostAndReplaceKeywords($mainpost_id, $sInputTemplate)
    {
        $sOutput = $sInputTemplate;
        $asReplace = [];

        $wp_mainpost = get_post($mainpost_id);
        // NOTE: input
        try {
            // NOTE: mandatory item
            array_push($asReplace, array(
                'key' => 'POST_TITLE',
                'value' => $wp_mainpost->post_title,
            ),
            array(
                'key' => 'FEATUREIMAGE',
                'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_mainpost->ID), 'single-post-thumbnail')[0],
            )
            );

            if (preg_match('/PER_POST/', $sInputTemplate)) {
                # place holder, not work yet
                array_push($asReplace, array(
                    'key' => 'PER_POST',
                    'value' => $this->fSection_Team_Post_Generate($mainpost_id),
                    )
                );
            }
            if ($wp_mainpost->post_content == '') {
            } else {
                $asReplace = array_merge($asReplace, fParseWPContentTable($wp_mainpost->post_content));
            }
            // NOTE: process
            $shouldRemoveMe = new cls_section();
            $sOutput = $shouldRemoveMe->fReplaceKeyWords($asReplace, $sOutput);
        } catch (Exception $e) {
            fHTML_varexport($e);
        }
        // NOTE: output
        return $sOutput;
    }

//support class for theme section
class cls_section
{
    public $sSectionTemplate = [];

    // reorder for display in wordpress post
    public function fSectionNull()
    {
        return '';
    }

    public function fHelloworld()
    {
    }

    public function pub_fHelloworld()
    {
    }

    public function fTestSkrollr()
    {
        $sTemp = <<<EOF_STEMP
<!--test_section_START-->
<section id="slide-1" style="height: 500px; padding: 0px;">
    <div class="bcg" data-bottom-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;" data-anchor-target="#slide-1">
    <!--<div data-center="background-color:rgb(0,0,255);" data-50-top="background-color:rgb(255,0,0);">WOOOT</div>-->
    </div>
</section>
<style type="text/css">
    .bcg {
        background-image: url(http://www.louislabs.com/wp-content/themes/littleredflower/css/skrollr/bcg_slide-1.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        height:650px;
        weight:100%;
    }
</style>
EOF_STEMP;

        // echo $sTemp;
        echo '';
    }
    // FIXME:60 pass 2nd parameters as array
    // private function fReplaceKeyWords($wpSection, &$wpPosts)
    public function fReplaceKeyWords($aInputReplace, $sHTMLInput)
    //public function fReplaceKeyWords($aInputReplace, $sHTMLInput)
    {
        $sOutput = $sHTMLInput;
        //input ->  section object -> grouped of wp_post object (by post_parent in wordpress)

        //process
        // TODO: if not found ?
        foreach ($aInputReplace as $InputReplace) {
            $sOutput = preg_replace('/<!--__'.$InputReplace['key'].'__-->/', $InputReplace['value'], $sOutput);
            // fHTML_varexport('/<!--__'.$InputReplace['key'].'__-->/');
        }
        //output -> HTML string with keyword replaced.

        return $sOutput;
    }

    // TODO:500 fSection_<PART>_Generate
    // TODO:510 fSection_<PART>_Post_Generate
    // TODO:550 fSection_Service_Post_Generate

    public function fSection_Service_Generate()
    {
        $Section_Service = <<<EOF_SSECTION_SERVICE
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Services</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
        </div>
        <div class="row text-center">
            <!--__PER_POST__-->
        </div>
    </div>
</section>
EOF_SSECTION_SERVICE;

        $sOutput = $sSection_Service;

        // TODO:910 remove me
        // return $Section_Service;
        return $sOutput;
    }
    // TODO:920 remove me


    // IDEA: post content as input parameters
    // public function fSection_Service_Post_Generate()
    public function fSection_Service_Post_Generate($parent_post_id)
    {
        $sOutput = '';
        $sTemp = '';

        // IDEA: template
$Section_Service_PerPost = <<<EOF_SSECTION_SERVICE_PERPOST
<!-- per post -->
<div class="col-md-4">
    <span class="fa-stack fa-4x">
        <i class="fa fa-circle fa-stack-2x text-primary"></i>
        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
    </span>
    <h4 class="service-heading"><!--__POST_TITLE__--></h4>
    <p class="text-muted">
        <!--__POST_CONTENT__-->
    </p>
</div>
<!-- per post end -->
EOF_SSECTION_SERVICE_PERPOST;

        // IDEA: get post content
        $args = array(
            'posts_per_page' => 99,
            'post_parent' => $parent_post_id,
            'post_type' => 'onepagepost',
            // IDEA: published ?
        );
        $section_sub_posts = get_posts($args);

        foreach ($section_sub_post  as $section_sub_posts) {
            if (false) {
                echo '<!--';
                echo 'findme';
                echo var_export($section_sub_post);
                echo '-->';
            }

            $sTemp = '';
            $sTemp = $Section_Service_PerPost;
            $sOutput = $sOutput.$sTemp;
        }
        // IDEA: replace $Section_Service_PerPost to content
        return $sOutput;
    }

    public function fSection_Portfolio_Generate($post_id)
    {
        // <!-- Portfolio Grid Section -->
        // IDEA: get post details by id

$Section_Portfolio = <<<EOF_SECTION_PORTFOLIO
<!-- Portfolio Grid Section -->
<section id="portfolio" class="bg-light-gray">
    <div id='<!--__ANCHOR__-->'><!--for html anchor positioning--></div>
    <div  class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><!--__POST_TITLE__--></h2>
                <h3 class="section-subheading text-muted"><!--__POST_CONTENT__--></h3>
            </div>
        </div>
        <div class="row">
            <!--__PER_POST__-->
        </div>
    </div>
</section>
EOF_SECTION_PORTFOLIO;

        $sOutput = '';
        $sOutput = $Section_Portfolio;

        $parent_post_id = $post_id;

        $sOutput = fFindAndReplaceAnchor($sOutput, $post_id);
        // TODO:610 function fReplaceKeyWords
        // $sOutput = preg_replace('/<!--__PER_POST__-->/', $this->fReplaceKeyWords('1','2'), $sOutput);

        // TODO: better handle here
        $post = get_post($post_id);
        $sOutput = preg_replace('/<!--__POST_TITLE__-->/', $post->post_title, $sOutput);
        $sOutput = preg_replace('/<!--__POST_CONTENT__-->/', $post->post_content, $sOutput);

        $sOutput = preg_replace('/<!--__PER_POST__-->/', $this->fSection_Portfolio_Post_Generate($parent_post_id), $sOutput);

        return $sOutput;
    }

    public function fSection_Portfolio_Post_Generate($parent_post_id)
    {
        $sOutput = '';

        $Section_Portfolio_PerPost = <<< EOF_SECTION_PORTFOLIO_PERPOST
<!-- per post -->
<div class="col-md-4 col-sm-6 portfolio-item">
    <a href="#portfolioModal<!--__iCounter__-->" class="portfolio-link add_shadow_3px" data-toggle="modal" style="max-height:260px;overflow:hidden;">
        <div class="portfolio-hover">
            <div class="portfolio-hover-content">
                <!--<i class="fa fa-plus fa-3x"></i>-->
                Read more
            </div>
        </div>
        <img src="<!--__FEATUREIMAGE__-->" class="img-responsive" alt="">
    </a>
    <div class="portfolio-caption add_shadow_3px" style="height:167px;">
        <h4><!--__POST_TITLE__--></h4>
        <p class="text-muted">
            <!--__cover_description__-->
        </p>
    </div>
</div>
<!-- per post end -->
EOF_SECTION_PORTFOLIO_PERPOST;

        if (false) {
            echo '<!--$parent_post_id:'.$parent_post_id.'-->';
        }
        $args = array(
            'posts_per_page' => 99,
            'post_parent' => $parent_post_id,
            'post_type' => 'onepagepost',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            // IDEA: published ?
        );
        $section_sub_posts = get_posts($args);

        if (false) {
            var_dump($section_sub_posts);
            die();
        }

        $iCounter = 0;
        foreach ($section_sub_posts as $section_sub_post) {
            if (false) {
                echo '<!--';
                var_dump($section_sub_post);
                echo '-->';
            }

            // TODO:930 remove me
            // var_dump(fParseWPContentTable($section_sub_post->post_content));
            // die();

            $aContent = fParseWPContentTable($section_sub_post->post_content);

            // IDEA: is key exist ? assume yes
            $bKeyExist = true;

            if ($bKeyExist) {
                $sTemp = '';
                $sTemp = $Section_Portfolio_PerPost;

            // NOTE: replace counter
            $sTemp = preg_replace('/<!--__iCounter__-->/', $iCounter++, $sTemp);

            // IDEA: field must exists
            $sTemp = preg_replace('/<!--__POST_TITLE__-->/', $section_sub_post->post_title, $sTemp);
                $feat_image = wp_get_attachment_url(get_post_thumbnail_id($section_sub_post->ID));

                $sTemp = preg_replace('/<!--__FEATUREIMAGE__-->/', $feat_image, $sTemp);

            // IDEA: field option/transform/not fixed
            for ($i = 0; $i < count($aContent); ++$i) {
                foreach ($aContent[$i] as $key => $value) {
                    if (false) {
                        var_export($aContent[$i]['key']);
                    }

                    $sTextToBeReplace = '<!--__'.$aContent[$i]['key'].'__-->';
                    $sTemp = preg_replace('/'.$sTextToBeReplace.'/', $aContent[$i]['value'], $sTemp);

                    if (false) {
                        echo '<!--search for : $aContent[$i][\'key\'] => '.$sTextToBeReplace.'-->';
                        die();
                    }
                }
            }
            }

            $sOutput = $sOutput.$sTemp;
        }

        // return $Section_Portfolio_PerPost;
        return $sOutput;
    }

    public function fSection_Portfolio_Modals_Generate($parent_post_id)
    {
        // <!-- Portfolio Modals -->
// <!-- Use the modals below to showcase details about your portfolio projects! -->

    $sOutput = '';

        $sTemplate = <<<EOF_STEMPLATE
<!-- Portfolio Modal <!--__iCounter__--> -->
<div class="portfolio-modal modal fade" id="portfolioModal<!--__iCounter__-->" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2><!--__main_title__--></h2>
                        <p class="item-intro text-muted">
                            <!--__main_subtitle__-->
                        </p>
                        <!--__main_description__-->
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
EOF_STEMPLATE;

        $post_Portfolio_posts = get_posts(array(
        'post_parent' => $parent_post_id,
        'post_type' => 'onepagepost',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => 99,
    ));

        $sTemp = '';
        $iCounter = 0;
        foreach ($post_Portfolio_posts as $post_Portfolio_post) {
            $sTemp = $sTemplate;

            $aContent = fParseWPContentTable($post_Portfolio_post->post_content);

        // IDEA: counter field
        $sTemp = preg_replace('/<!--__iCounter__-->/', (string) $iCounter++, $sTemp);

        // IDEA: field must exists
        $sTemp = preg_replace('/<!--__PER_POST__-->/', $this->fSection_Portfolio_Post_Generate($parent_post_id), $sTemp);
            $sTemp = preg_replace('/<!--__POST_TITLE__-->/', $section_sub_post->post_title, $sTemp);
            $feat_image = wp_get_attachment_url(get_post_thumbnail_id($section_sub_post->ID));
            $sTemp = preg_replace('/<!--__FEATUREIMAGE__-->/', $feat_image, $sTemp);

        // TODO:700 lastend
        // IDEA: field option/transform/not fixed
        for ($i = 0; $i < count($aContent); ++$i) {
            foreach ($aContent[$i] as $key => $value) {
                if (false) {
                    echo fHTML_varexport($aContent[$i]);
                // die();
                }
            // TODO:490 exceptions ?

            if (false) {
            } elseif ($aContent[$i]['key'] == 'main_description') {
                // TODO:600 for main_description

                // TODO:10 <img> tag need to have append class
                if ($aContent[$i]['value'] == '') {
                } else {
                    $sHTML = $aContent[$i]['value'];
                    $checkforimgtag = new DOMDocument();
                    // $checkforimgtag->loadHTML($sHTML, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                    $checkforimgtag->loadHTML($sHTML,  LIBXML_HTML_NODEFDTD);

                    // fHTML_varexport($sHTML);

                    // $img = $checkforimgtag->getElementsByTagName('a')->item(0);
                    // IDEA: how about more than 1 images ??
                    $img = $checkforimgtag->getElementsByTagName('img')->item(0);
                    $img_src = $img->attributes->getNamedItem('src')->value;

                    #NOTE append to attribute class
                    $sTemp_GetAttribute = $img->getAttribute('class');
                    $img->setAttribute('class', $sTemp_GetAttribute.' img-responsive img-centered ');

                    // TODO:940 remove me
                    // echo fHTML_varexport($checkforimgtag->saveHTML($img));

                    // NOTE: it comes with <html><body>xxx...</body></html> by default. To remove it.
                    $sTemp_message_description = $checkforimgtag->saveHTML();
                    $sTemp_message_description = preg_replace('/<html><body>/', '', $sTemp_message_description);
                    $sTemp_message_description = preg_replace('/<\/body><\/html>/', '', $sTemp_message_description);
                    //echo fHTML_varexport($sTemp_message_description);
                    // die();

                    // TODO: test for programmers_badge
                    $sTemp_message_description = $sHTML;
                    // echo fConvBadge($sHTML);
                    // die();

                    // IDEA: shall i use wordpress hook ??
                    $sTemp_message_description = fConvBadge($sHTML);

                    // IDEA: sent the text(conditioned) back to the html body
                    $sTextToBeReplace = '<!--__'.$aContent[$i]['key'].'__-->';
                    $sTemp = preg_replace('/'.$sTextToBeReplace.'/', $sTemp_message_description, $sTemp);
                }
            } else {
                $sTextToBeReplace = '<!--__'.$aContent[$i]['key'].'__-->';
                $sTemp = preg_replace('/'.$sTextToBeReplace.'/', $aContent[$i]['value'], $sTemp);
            }

                if (false) {
                    echo fHTML_varexport(var_export($aContent[$i]));
                    echo fHTML_varexport('<!--search for : $aContent[$i][\'key\'] => '.$sTextToBeReplace.'-->');
                    echo fHTML_varexport('<!--search for : $aContent[$i][\'value\'] => '.$aContent[$i]['value'].'-->');
                    echo fHTML_varexport($sTemp);
                    die();
                }
            }
        }
            $sOutput = $sOutput.$sTemp;
        }

        return $sOutput;
    }

    public function fSection_About_Generate($mainpost_id)
    {
        # code...
$Section_About = <<<EOF_SECTION_ABOUT
<!-- About Section -->
<section id="about">
    <div id='<!--__ANCHOR__-->'><!--for html anchor positioning--></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><!--__POST_TITLE__--></h2>
                <h3 class="section-subheading text-muted"><!--__POST_CONTENT__--></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <!--__PER_POST__-->
                </ul>
            </div>
        </div>
    </div>
</section>
EOF_SECTION_ABOUT;

        $sOutput = $Section_About;
        $asReplace = [];

        $wp_mainpost = get_post($mainpost_id);
    // NOTE: input
    array_push($asReplace,
        array(
            'key' => 'POST_TITLE',
            'value' => $wp_mainpost->post_title,
        ),
        array(
            'key' => 'PER_POST',
            'value' => $this->fSection_About_Post_Generate($mainpost_id),
        )
    );
        $asReplace = array_merge($asReplace, fParseWPContentTable($wp_mainpost->post_content));

    // NOTE: process
        $sOutput = fFindAndReplaceAnchor($sOutput, $wp_mainpost->ID);

        $sOutput = $this->fReplaceKeyWords($asReplace, $sOutput);

    // NOTE: output
    return $sOutput;
    }

    public function fSection_About_Post_Generate($parentpost_id)
    {
        # code...
      // TODO:520 fSection_About_Post_Generate
$Section_About_PerPost = <<< EOF_SECTION_ABOUT_PERPOST
<!-- per post -->
<!--__TIMELINE_INVERTED__-->
  <!--<div class="timeline-image">-->
  <div class="timeline-image" style="background-color: #ffffff">
      <!--__TIMELINE_IMAGE_CONTENT__-->
      <!--<img class="img-circle img-responsive" src="<!--__FEATUREIMAGE__-->" alt="<!--__FEATUREIMAGE_ALT__-->">-->
      <!--<div class="img-circle img-responsive" style="background:url(<!--__FEATUREIMAGE__-->) no-repeat center center;  height:100%; width: 100%; background-size:contain ">.</div>-->
  </div>
  <div class="timeline-panel">
      <div class="timeline-heading">
          <h4><!--__POST_TITLE__--></h4>
          <h4 class="subheading"><!--__TIMELINE_SUBHEADING__--></h4>
      </div>
      <div class="timeline-body">
          <p class="text-muted" ><!--__TIMELINE_BODY__--></p>
          <!--__MORE_DESCRIPTION_BUTTON__-->
      </div>
  </div>
</li>
<!-- per post end -->
EOF_SECTION_ABOUT_PERPOST;

        $Section_About_PerPost_start = <<< EOF_SECTION_ABOUT_PERPOSTSTART
<!-- per post -->
<li class="timeline-inverted">
    <div class="outside timeline-image">
        <!--<h4 style="padding: 25px; margin: auto; position: absolute; left:0; right: 0; top: 0; bottom: 0;"><!--__POST_TITLE__--></h4>-->
        <div class="inside">
            <h4 style="margin-top: 0px; margin-bottom: 0px;"><!--__POST_TITLE__--></h4>
        </div>
    </div>
</li>
<!-- per post end -->
EOF_SECTION_ABOUT_PERPOSTSTART;

        $Section_About_PostDetails = <<< EOF_SECTION_ABOUT_POSTDETAILS
<!-- per about post details start-->
<div class="portfolio-modal modal fade" id="timelineModal<!--__iCounter__-->" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Timeline Details Go Here -->
                        <!--__timeline_dialog_details__-->
                        <!--__MORE_DESCRIPTION__-->
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Timeline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- per about post details end-->
EOF_SECTION_ABOUT_POSTDETAILS;

        $sOutput = '';
        $wp_subposts = get_posts(array(
            'posts_per_page' => 99,
            'post_parent' => $parentpost_id,
            'post_type' => 'onepagepost',
            'orderby' => 'menu_order',
            'order' => 'ASC',
        ));

        // fHTML_varexport($wp_subposts);
        // die();

        $i = 0;
        $j = 0;
        foreach ($wp_subposts as $wp_subpost) {
            // NOTE: input

            // echo 'findme';
            // fHTML_varexport($wp_subpost);

            $j = $j + 1;
            // $tempSectionAboutPostDetails = $Section_About_PostDetails;

            if ($i == 0) {
                $sTemp = $Section_About_PerPost_start;
                $i = 1;
            } else {
                $sTemp = $Section_About_PerPost;

                if ($i == 1) {
                    #llaw, left right alternation.
                    # left
                    $stimelineLeftRightControl = '<li>';
                    $i = 2;
                } else {
                    $stimelineLeftRightControl = '<li class="timeline-inverted">';
                    $i = 1;
                }
            }
            $asReplace = [];
            array_push($asReplace,
                array(
                    'key' => 'POST_TITLE',
                    'value' => $wp_subpost->post_title,
                ),
                array(
                    'key' => 'FEATUREIMAGE',
                    'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_subpost->ID), 'single-post-thumbnail')[0],
                ),
                array(
                    'key' => 'TIMELINE_INVERTED',
                    'value' => $stimelineLeftRightControl,
                ),
                array(
                    'key' => 'iCounter',
                    'value' => $j,
                )
            );

            // fHTML_varexport($wp_subpost->post_title);
            // fHTML_varexport(get_post_meta(get_post_thumbnail_id($wp_subpost->ID, '_wp_attachment_image_alt', true)));
            // die();

            // NOTE: handling for different filetype.

            foreach ($asReplace as $sReplace) {
                if ($sReplace['key'] == 'FEATUREIMAGE') {
                    $stemp = '';
                    if ($sReplace['value'] == false) {
                        //if no FEATUREIMAGE
                        // fHTML_varexport($wp_subpost->post_title);
                        // die();
                    } else {

                        // TODO: solve problem between png and svg
                        // if (strpos($sReplace['value'], 'svg') > 0) {
                        if (true) {

                            // NOTE: for SVG
                            // <div style="background:url(<!--__FEATUREIMAGE__-->) no-repeat center center;  height:100%; width: 100%; background-size:contain "></div>

                            // TODO: make tempolate here
                            // $stemp = '<div class="img-circle img-responsive" style="background:url('.$sReplace['value'].') no-repeat center center;  height:100%; width: 100%; background-size:contain "></div>';
                            $stemp = '<div class="img-circle img-responsive" style="background:url('.$sReplace['value'].') no-repeat center center;  height: 96%; width: 96%; margin-left: 2%; margin-top: 2%; background-size:contain "></div>';
                        } else {
                            $stemp = '<img class="img-circle img-responsive" src="'.$sReplace['value'].'" alt="<!--__FEATUREIMAGE_ALT__-->">';
                        }
                        // <img class="img-circle img-responsive" src="<!--__FEATUREIMAGE__-->" alt="">

                        $stemp = '<a data-original-title="? ? ?" data-toggle="tooltip" href="<!--__FEATUREIMAGE_HREF__-->" target="_blank">'.$stemp.'</a>';
                    }

                    array_push($asReplace,
                        array(
                            'key' => 'TIMELINE_IMAGE_CONTENT',
                            'value' => $stemp,
                        )
                    );
                }
            }

            // fHTML_varexport($asReplace);
            // fHTML_varexport($asReplace);
            // fHTML_varexport(strpos($asReplace['FEATUREIMAGE'],'svg')>0);
            // echo 'findme';

            $asReplace = array_merge($asReplace, fParseWPContentTable($wp_subpost->post_content));
            // fHTML_varexport($asReplace);
            // echo 'findme';
            // after parse table?


            // NOTE: output
            // NOTE: generate timeline label on main page

            // NOTE: generate timeline details on MORE button
            // NOTE: if MORE_DESCRIPTION appears in the post['key']
            $bShouldGenPostDetails = false;
            foreach ($asReplace as $sReplace) {
                if ($sReplace['key'] == 'MORE_DESCRIPTION') {

                    // TODO: tidy up here
                    // NOTE: generate MORE button if the MORE_DESCRIPTION appears in the post['key']
                    $sMoreDescriptionButtonHTML = '<div style="padding-top:20px;"><button class="btn btn-primary" data-toggle="modal" data-target="#timelineModal'.$j.'">know more ?</button></div>';
                    array_push($asReplace, array(
                                        'key' => 'MORE_DESCRIPTION_BUTTON',
                                        'value' => $sMoreDescriptionButtonHTML,
                                    ));

                    $bShouldGenPostDetails = true;
                    //
                    // $a = 'i我';
                    // echo $a;
                    // fHTML_varexport($asReplace);

                    $tempSectionAboutPostDetails = $this->fReplaceKeyWords($asReplace, $Section_About_PostDetails);

                    // NOTE: test fConvRFC
                    // echo '21800000';
                    // fHTML_varexport($tempSectionAboutPostDetails);

                    // NOTE: output conditioning
                    $tempSectionAboutPostDetails = fReplaceKeywordsWithLink($tempSectionAboutPostDetails);
                    $tempSectionAboutPostDetails = fConvRFC($tempSectionAboutPostDetails);

                    $GLOBALS['timeline_models'] .= $tempSectionAboutPostDetails;
                } else {
                    // echo $sReplace
                }
            }
            $sTemp = $this->fReplaceKeyWords($asReplace, $sTemp);
            $sOutput = $sTemp.$sOutput;
        }

        return $sOutput;
    }

    public function fSection_Team_Generate($mainpost_id)
    {
        # code...
        // TODO:560 fSection_Team_Generate
$Section_Team = <<<EOF_SECTION_TEAM
<!-- Team Section -->
<section id="team" class="bg-light-gray">
    <div id='<!--__ANCHOR__-->'><!--for html anchor positioning--></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><!--__POST_TITLE__--></h2>
                <h3 class="section-subheading text-muted"><!--__SUB_HEADING__--></h3>
            </div>
        </div>
        <!--__PER_POST__-->
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <p class="large text-muted">
                    <!--__BOTTOM_TEXT__-->
                </p>
            </div>
        </div>
    </div>
</section>
EOF_SECTION_TEAM;

        $sOutput = $Section_Team;
        $asReplace = [];

        $wp_mainpost = get_post($mainpost_id);
    // NOTE: input
    array_push($asReplace,
        array(
            'key' => 'POST_TITLE',
            'value' => $wp_mainpost->post_title,
        ),
        array(
            'key' => 'PER_POST',
            'value' => $this->fSection_Team_Post_Generate($mainpost_id),
        )
    );
        $asReplace = array_merge($asReplace, fParseWPContentTable($wp_mainpost->post_content));

        // NOTE: process
        $sOutput = fFindAndReplaceAnchor($sOutput, $wp_mainpost->ID);

        $sOutput = $this->fReplaceKeyWords($asReplace, $sOutput);

        // NOTE: output
        return $sOutput;
    }

    public function fSection_Team_Post_Generate($parentpost_id)
    {
        $Section_Team_PerPost = <<<EOF_SECTION_TEAM_PERPOST
<!-- per post -->
<div class="col-sm-4">
    <div class="team-member">
        <img src="<!--__FEATUREIMAGE__-->" class="img-responsive img-circle" alt="">
        <h4><!--__POST_TITLE__--></h4>
        <p class="text-muted">
            <!--__POST_CONTENT__-->
        </p>
        <ul class="list-inline social-buttons">
            <li><a href="#"><i class="fa fa-twitter"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a>
            </li>
        </ul>
    </div>
</div>
<!-- per post end -->
EOF_SECTION_TEAM_PERPOST;

        $sOutput = '';

        $wp_subposts = get_posts(array(
            'posts_per_page' => 99,
            'post_parent' => $parentpost_id,
            'post_type' => 'onepagepost',
        ));
        foreach ($wp_subposts as $wp_subpost) {
            $sTemp = $Section_Team_PerPost;

            $asReplace = [];
            array_push($asReplace,
                array(
                    'key' => 'POST_TITLE',
                    'value' => $wp_subpost->post_title,
                ),
                array(
                    'key' => 'POST_CONTENT',
                    'value' => $wp_subpost->post_content,
                ),
                array(
                    'key' => 'FEATUREIMAGE',
                    'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_subpost->ID), 'single-post-thumbnail')[0],
                )
            );
            // $asReplace=array_merge($asReplace,fParseWPContentTable($wp_mainpost->post_content));

            // NOTE: process
            $sTemp = $this->fReplaceKeyWords($asReplace, $sTemp);
            $sOutput = $sOutput.$sTemp;
        }

        return $sOutput;
    }

    public function fSection_Contact_Generate($mainpost_id)
    {
        # code...
        // TODO:530 fSection_Contact_Generate

        $sOutput = '';

        $SectionContact = <<<EOF_SECTIONCONTACT
<!-- Contact Section -->
<section id="contact" style="background-image: url(<!--__FEATUREIMAGE__-->)">
    <div id='<!--__ANCHOR__-->'><!--for html anchor positioning--></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><!--__POST_TITLE__--></h2>
                <h3 class="section-subheading text-muted"><!--__POST_CONTENT__--></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
EOF_SECTIONCONTACT;

        $sOutput = $SectionContact;

        try {
            if (false) {
                $wp_mainpost = get_post($mainpost_id);

                $asReplace = [];
                array_push($asReplace,
                array(
                    'key' => 'POST_TITLE',
                    'value' => $wp_mainpost->post_title,
                ),
                array(
                    'key' => 'POST_CONTENT',
                    'value' => $wp_mainpost->post_content,
                ),
                array(
                    'key' => 'FEATUREIMAGE',
                    // 'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_mainpost->ID, 'single-post-thumbnail'))[0],
                    'value' => wp_get_attachment_image_src(get_post_thumbnail_id($wp_mainpost->ID))[0],
                )
                );

                // fHTML_varexport($wp_mainpost);

                // $asReplace = array_merge($asReplace, fParseWPContentTable($wp_mainpost->post_content));

                // NOTE: process
                $sOutput = fFindAndReplaceAnchor($sOutput, $wp_mainpost->ID);

                $sOutput = $this->fReplaceKeyWords($asReplace, $sOutput);
            }

            $mainposts = get_posts(array(
                'post_type' => 'onepagepost',
                'name' => '_pageheader',
            ));
            $sOutput = fGetPostAndReplaceKeywords($mainpost_id, $sOutput);
        } catch (Exception $e) {
        }

        return $sOutput;
    }

    public function fSection_Contact_Post_Generate()
    {
        # code...
      // TODO:540 fSection_Contact_Post_Generate


    return $SectionContact;
    }

    public function fSection_pagefooter_Post_Generate()
    {
        // NOTE: custom properties "section_theme" should be "fSection_pagefooter_Post_Generate"
        // NOTE: hardcode for onepagepost->pagefooter
        // NOTE: page title hardcoded to _pagefooter

$Section_pagefooter = <<<EOF_SECTIONPAGEFOOTER
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; Your Website 2014</span><br/>
                <span class="copyright"><a href="http://blackrockdigital.github.io/startbootstrap-agency/"></a>thanks opensource and their effort...</a></span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <!--__SOCIAL_BUTTONS__-->
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <!--__QUICKLINKS__-->
                </ul>
            </div>
        </div>
    </div>
</footer>
EOF_SECTIONPAGEFOOTER;

    // NOTE: input
    $sOutput = '';
        $sOutput = $Section_pagefooter;

        $wp_pagefooters = get_posts(array(
        'post_type' => 'onepagepost',
        'name' => '_pagefooter',
        ));

        // NOTE: only 1 post expected
        $wp_pagefooter = $wp_pagefooters[0];
        // fHTML_varexport($wp_pagefooter);

        $asReplace = [];
        array_push($asReplace, array(
        'key' => 'POST_TITLE',
        'value' => $wp_pagefooter->post_title,
        ));
        $aContent = [];
        $aContent = fParseWPContentTable($wp_pagefooter->post_content);

    // NOTE: process
    // NOTE: special arrangment for faicons and quicklinks
    // fHTML_varexport($aContent);

    // NOTE: massage content before pass to replace
    foreach ($aContent as &$Content) {
        // fHTML_varexport('findme1');
        // fHTML_varexport($Content);
        if ($Content['key'] == 'SOCIAL_BUTTONS') {
            $aSocialLink = explode("\r\n", $Content['value']);
            // $aSocialLink = explode("<br />", $Content['value']);

            // NOTE: expected input [fa-icon,fa-iconsize,hreflink]
            // NOTE: expected input [fa-twitter,fa-5,twitterlink][fa-whatsapp,fa-5,facebooklink][fa-linkedin,fa-5,likedinlink][fa-twitter,fa-5,twitterlink][fa-facebook,fa-5,facebooklink][fa-facebook,fa-5,likedinlink]
            preg_match_all('/\[(.+?),(.+?),(.+?)\]/', $Content['value'], $aSocialLink);

            $Content['value'] = $this->fGenerateSocialButtons($aSocialLink);
        } elseif ($Content['key'] == 'QUICKLINKS') {
            // fGenerateQuickLink
            $sQuicklinks = explode("\r\n", $Content['value']);
            $Content['value'] = $this->fGenerateQuickLink($sQuicklinks);
        } else {
            // NOTE: why i m here ?
        }
    }
        $asReplace = array_merge($asReplace, $aContent);

        $sOutput = $this->fReplaceKeyWords($asReplace, $sOutput);
    // echo 'findme';
    // fHTML_varexport($asReplace);
    // fHTML_varexport($sOutput);
    // die();

    // NOTE: output


        return $sOutput;
    }
    public function fGenerateQuickLink($asQuickLinks)
    {
        $sOutput = '';
        $sTemplate = "\r\n".'<li><a href="<!--__QUICKINKHREF__-->"><!--__QUICKLINKTEXT__--></a></li>'."\r\n";

        foreach ($asQuickLinks as $sQuicklink) {
            $sHref = explode(',', $sQuicklink)[0];
            $sText = explode(',', $sQuicklink)[1];

            $sTemp = $sTemplate;
            $sTemp = preg_replace('/<!--__QUICKINKHREF__-->/', $sHref, $sTemp);
            $sTemp = preg_replace('/<!--__QUICKLINKTEXT__-->/', $sText, $sTemp);

            $sOutput = $sOutput.$sTemp;
        }

        return $sOutput;
    }

    public function fPrintDebug()
    {
        echo 'findme ';
        echo 'printdebug';
    }

    public function fGenerateSocialButtons($asSocialButtons)
    {
        // NOTE: constant
        $sOutput = '';
        $sTemplate = <<<EOF_STEMPLATE
<li><a href="<!--__SICONS_LINK__-->"><i class="fa <!--__SICONS__--> <!--__SICONS_SIZE__-->"></i></a></li>
EOF_STEMPLATE;

        // NOTE: input
        // array (
        //   0 =>
        //   array (
        //     0 => '[fa-icon,fa-iconsize,hreflink]',
        //     1 => '[fa-icon,fa-iconsize,hreflink]',
        //   ),
        //   1 =>
        //   array (
        //     0 => 'fa-icon',
        //     1 => 'fa-icon',
        //   ),
        //   2 =>
        //   array (
        //     0 => 'fa-iconsize',
        //     1 => 'fa-iconsize',
        //   ),
        //   3 =>
        //   array (
        //     0 => 'hreflink',
        //     1 => 'hreflink',
        //   ),
        // )


        // NOTE: process
        // NOTE: output

        // TODO: empty list?
        // TODO: icon not exist ?
        for ($i = 0; $i < count($asSocialButtons[0]); ++$i) {
            // NOTE: input
            $sIcon = $asSocialButtons[1][$i];
            $sIconSize = $asSocialButtons[2][$i];
            $sLink = $asSocialButtons[3][$i];

            // NOTE: process
            $sTemp = $sTemplate;
            $sTemp = preg_replace('/<!--__SICONS__-->/', $sIcon, $sTemp);
            $sTemp = preg_replace('/<!--__SICONS_SIZE__-->/', $sIconSize, $sTemp);
            $sTemp = preg_replace('/<!--__SICONS_LINK__-->/', $sLink, $sTemp);

            // NOTE: output
            $sOutput = $sOutput.$sTemp;
        }
        // foreach ($asSocialButtons as $sSocialButton) {
            // NOTE: input
            // $sIcon = explode(',', $sSocialButton)[0];
            // $sLink = explode(',', $sSocialButton)[1];
            //
            // // fHTML_varexport(explode(',', $sSocialButton));
            //
            // // NOTE: process
            // $sTemp = $sTemplate;
            // $sTemp = preg_replace('/<!--__SICONS__-->/', $sIcon, $sTemp);
            // $sTemp = preg_replace('/<!--__SICONS_LINK__-->/', $sLink, $sTemp);
            //
            // // NOTE: output
            // $sOutput = $sOutput.$sTemp;
        // }

        return $sOutput;
    }

    public function __construct()
    {
    }
}
