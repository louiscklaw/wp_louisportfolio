<?php
    #NOTE for debug
    include dirname(__FILE__).'/../cust_functions.php';

    require_once dirname(__FILE__).'/../PHP-MySQLi-Database-Class/MysqliDb.php';
    // include dirname(__FILE__).'/../../config.php';
    // include dirname(__FILE__).'/../config.php';
    // echo dirname(__FILE__);

    // for google recapcha
    require_once 'recaptchalib.php';
    // your secret key
    $secret = "6LdpSxcTAAAAAOzaqT5ZLm_yqh8SfHPAoA_I3ZNR";

    // empty response
    $response = null;

    // check secret key
    $reCaptcha = new ReCaptcha($secret);
    // if submitted check response
    if ($_POST["g-recaptcha-response"]) {
        $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
    }else{
        // the g-recaptcha-response is not found
        f_DB_error_log('the g-recaptcha-response was not found');
    }

    #NOTE add to mail list process page
    // var_dump($_POST);


    if ($response != null && $response->success) {
        //   echo "Hi " . $_POST["name"] . " (" . $_POST["email"] . "), thanks for submitting the form!";
        f_DB_error_log('process.php start');
        #input
        $sEmail = $_POST['email'];
        f_DB_error_log('start process.php');
        // fHTML_varexport($_POST);
        if (isset($_POST['email'])){
          f_DB_error_log('init DB connection');
          $db = new MysqliDb('localhost', 'littleredflower', 'littleredflower','littleredflower');
          $db->setPrefix('user_');
          // TODO: search for not exist ???
          $id = $db->insert('waiting_email_list',
              array(
                  'email_address' => var_export($sEmail, true),
                  )
          );
        }
        f_DB_error_log('process.php end');
    } else {
        f_DB_error_log('the $response is not valid');
    }

?>
