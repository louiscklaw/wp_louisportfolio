<?php
    if (function_exists('fLogServerVar'))    {
    } else {
        function fLogServerVar($iInput)
        {
            #include_once $GLOBALS['DIR_INC'].'/cust_functions.php';
            require_once dirname(__FILE__).'/PHP-MySQLi-Database-Class/MysqliDb.php';

            $db = new MysqliDb('localhost', $GLOBALS['DB_USER'], $GLOBALS['DB_PASS'], $GLOBALS['DB_NAME']);

            $db->setPrefix('user_');
            $id = $db->insert('http_request_log',
                array(
                    'content'              => var_export($iInput, true),
                    'HTTP_USER_AGENT'      => array_key_exists('HTTP_USER_AGENT', $iInput) ? var_export($iInput['HTTP_USER_AGENT'], true):'not set',
                    'HTTP_ACCEPT_LANGUAGE' => array_key_exists('HTTP_ACCEPT_LANGUAGE', $iInput) ? var_export($iInput['HTTP_ACCEPT_LANGUAGE'], true):'not set',
                    'HTTP_REFERER'         => array_key_exists('HTTP_REFERER', $iInput) ? var_export($iInput['HTTP_REFERER'], true):'not set',
                    'page_visited'         => 'louislabs_waiting',
                    )
            );
        }
    }
