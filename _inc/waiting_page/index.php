<?php
  include get_template_directory().'/config.php';

  include dirname(__FILE__).'/../cust_functions.php';

  // TODO: remove comment
  // include 'http_request_log.php';
  // fLogServerVar($_SERVER);

  $CWD = get_template_directory_uri().'/_inc/waiting_page';

  // NOTE: grep "_waiting_page", convert parameters for html body use
  $aTemp = array();
  foreach ($GLOBALS['allposts'] as $idx => $post) {
      if ($post->post_title == '_waiting_page') {
          $aTemp = fParseWPContentTable($post->post_content);
          break;
      } else {
      }
  }
  $page_variable = array();
  foreach ($aTemp as $idx => $keypair) {
      $page_variable[$keypair['key']] = $keypair['value'];
  }
?>
<?php
// handling of html tag move to main index.php
// <!DOCTYPE html>
// <html lang="en">
?>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Free coming soon template with jQuery countdown">
    <meta name="author" content="http://bootstraptaste.com">

    <!-- <meta charset="utf-8"> -->
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <!-- <meta name="keywords" content="portfolio, programmer, developer"> -->
    <?php include($GLOBALS['DIR_INC'].'/meta.php') ?>

    <base href="<?php echo $CWD ?>/" target="_blank">


    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!--apple icon?-->

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
  	<link href="assets/css/bootstrap-theme.css" rel="stylesheet">

    <!-- simple style -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/waitingpage.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <!-- sweet alert -->
    <script src="dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">

    <!-- Trianglify -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.4.0/trianglify.min.js"></script>

    <title><?php echo get_bloginfo('name');  ?></title>
  </head>

  <body  class="bg_blur">
  	<div id="wrapper">
      <div class="dim_bg"></div>
  		<div class="container">
  			<div class="row">
  				<div class="col-sm-12 col-md-12 col-lg-12">
  					<h1>Louis portfolio webpage</h1>
  					<h2 class="subtitle">I'm working hard to improve website and ready to launch after</h2>
  					<div id="countdown"></div>
  					<!-- <form class="form-inline signup" role="form"> -->
            <!-- <form action="<?php echo "{$CWD}";?>/process.php" method="post" role="form" class="contactForm"> -->
  				</div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12" ></div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12" >
            <form id="emailform">
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <div class="form-group">
                    <input style="text-align:center" type="email" name="email" id="email" class="form-control" placeholder="email address here please ~~~">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="btn g-recaptcha" data-callback="cb_recaptcha" data-sitekey="6LdpSxcTAAAAALwsku50AllYEoAFhpvBGskWVu81"></div>
              </div>
              <div class="row">
                <button id="id_button" class="btn btn-theme" disabled>Get notified!</button>
              </div>
    				  <!-- <button type="submit" class="btn btn-theme">Get notified!</button> -->
    				</form>
          </div>
        </div>
  			<div class="row">
  				<div class="col-lg-6 col-lg-offset-3">
  						<p class="copyright">
                Copyright &copy; 2016. Al rights reserved. <a href="http://bootstraptaste.com/">Bootstrap Themes</a> by BootstrapTaste
              </p>
              <!--
                  All links in the footer should remain intact.
                  Licenseing information is available at: http://bootstraptaste.com/license/
                  You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=WeBuild
              -->
  				</div>
  			</div>
  		</div>
  	</div>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
  	<script src="assets/js/jquery.countdown.min.js"></script>
  	<script type="text/javascript">
      $('#countdown').countdown('<?php echo $page_variable['countdown']; ?>', function(event) {
        $(this).html(event.strftime('%w weeks %d days <br /> %H:%M:%S'));
      });

      //Passing an anonymous function as the event handler.

      // magic.js
      $(document).ready(function() {
          // process the form
          $('form').submit(function(event) {
              // get the form data
              // there are many ways to get this data using jQuery (you can use the class or id also)
              var formData = {
                  // 'name'              : $('input[name=name]').val(),
                  // 'superheroAlias'    : $('input[name=superheroAlias]').val()
                  'email'                : $('input[name=email]').val(),
                  'g-recaptcha-response' : $('.g-recaptcha-response').val()
              };
              $.ajax({
                  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url         : 'http://www.louislabs.com/wp-content/themes/littleredflower/_inc/waiting_page/process.php', // the url where we want to POST
                  data        : formData, // our data object
                  dataType    : 'json', // what type of data do we expect back from the server
                  encode          : true
              }).done(function() {
                  alert( "success" );
                })
                .fail(function() {
                  // alert( "error" );
                })
                .always(function() {
                  // alert( "complete" );
                  swal("Thanks!", "i will inform you once complete", "success");

                  $('#id_button').html('Done');
                  $('#id_button').prop('disabled','disabled');
                });
              // swal("Good job!", "You clicked the button!", "success");
              // stop the form from submitting the normal way and refreshing the page
              event.preventDefault();
          });
      });
    </script>

    <script type="text/javascript">
      var D = document;

      $(document).ready(
        function(){

          targetHeight= Math.max(
              D.body.scrollHeight, D.documentElement.scrollHeight,
              D.body.offsetHeight, D.documentElement.offsetHeight,
              D.body.clientHeight, D.documentElement.clientHeight
          );
          var pattern = Trianglify({
              width: $(document).width(),
              height: targetHeight
          });

          $("body").css('background-image',' url(  '+pattern.png()+'  )');
          $("body").css('background-repeat','no-repeat');
          $("body").css('background-position','center center');
          $("body").css('background-attachment','fixed');
          $(".dim_bg").css('min-height',$(document).height());
          console.log($(document).height());
          console.log('test document ready');

          $(document).on("scroll", fReCalcHeight);
          console.log('register background event');
        }
      );

      function fReCalcHeight(){
        targetHeight= Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );
        $(".dim_bg").css('min-height',targetHeight);
        $(document).off("scroll", fReCalcHeight);
        console.log('fReCalcHeight fired');
      }

      // fix firefox bug
      $(document).scroll(function(){
        // fReCalcHeight();
      });

      $(window).resize(function(){
        fReCalcHeight();
        console.log('fReCalcHeight fired');
      });
    </script>
    <!-- google recapcha -->
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
    <script>
      function cb_recaptcha() {
        if ($('#id_button').prop('disabled') && $('#id_button').text()=="Get notified!"){
          $('#id_button').removeAttr('disabled');
        }
      };
    </script>
  </body>
<?php
// handling of html tag move to index.php
// </html>
 ?>
