

<!-- Trianglify -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.4.0/trianglify.min.js"></script>



<script type="text/javascript">
  var D = document;
  $(document).ready(
    function(){
      targetHeight= Math.max(
          D.body.scrollHeight, D.documentElement.scrollHeight,
          D.body.offsetHeight, D.documentElement.offsetHeight,
          D.body.clientHeight, D.documentElement.clientHeight
      );
      var pattern = Trianglify({
          width: $(document).width(),
          height: targetHeight
      });

      $("header").css('background-image',' url(  '+pattern.png()+'  )');
      $("header").css('background-repeat','no-repeat');
      $("header").css('background-position','center center');
      $("header").css('background-attachment','fixed');
      // $(".dim_bg").css('min-height',$(document).height());
      // console.log($(document).height());
      console.log('header trianglify ready');

      // $(document).on("scroll", fReCalcHeight);
      // console.log('register background event');
    }
  );
</script>
