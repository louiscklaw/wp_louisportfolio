<?php
    $array = array('post_parent' => 0, 'post_type' => 'onepagepost');
    $mainpost = get_posts($array);

    // TODO: if not found??
    if (count($mainpost) > 0) {
        $sTitle = $mainpost[0]->post_title;
    }
?>
<?php echo '<!-- head.php start -->';?>
<title><?php echo get_bloginfo('name'); ?></title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo $GLOBALS['URI_CSS'];?>/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo $GLOBALS['URI_CSS'];?>/agency.css" rel="stylesheet">
<link href="<?php echo $GLOBALS['URI_CSS'];?>/cust_css.css" rel="stylesheet">

<!-- Custom Fonts -->
<!-- Google Fonts -->
<link href="<?php echo $GLOBALS['PATH']['ROOT']['URI']; ?>font-awesome/css/font-awesome.min.css" rel ="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"                          rel ="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script'                              rel ='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'     rel ='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700'                 rel ='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Poiret+One'                                  rel ='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Quicksand'                                   rel ='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'                                     rel ='stylesheet' type='text/css'>


<link rel="author" href="https://www.facebook.com/louislabs"/>

<!-- sumome traffic track -->
<script src="//load.sumome.com/" data-sumo-site-id="a9e3ce06533ae9dc8e5b5f74cadf224b37375ab559be83e4ac41cbaf55e3c52d" async="async"></script>

<!-- Trianglify -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.4.0/trianglify.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php include $GLOBALS['DIR_INC'].'/favicon.php'; ?>


<?php echo '<!-- head.php end -->';?>
