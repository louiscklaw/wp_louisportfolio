<?php

include dirname(__FILE__).'/../config.php';

// TODO: remove me
// include $GLOBALS['DIR_INC'].'/cls_section.php';

if (class_exists('cls_section')) {
} else {
    include $GLOBALS['DIR_INC'].'/cls_section.php';
}

function OnePagePost_register()
{
    $labels = array(
        'name' => _x('OnePagePosts', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('OnePagePost', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('OnePagePosts', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('OnePagePost', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Add New', 'OnePagePost', 'your-plugin-textdomain'),
        'add_new_item' => __('Add New OnePagePost', 'your-plugin-textdomain'),
        'new_item' => __('New OnePagePost', 'your-plugin-textdomain'),
        'edit_item' => __('Edit OnePagePost', 'your-plugin-textdomain'),
        'view_item' => __('View OnePagePost', 'your-plugin-textdomain'),
        'all_items' => __('All OnePagePosts', 'your-plugin-textdomain'),
        'search_items' => __('Search OnePagePosts', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent OnePagePosts:', 'your-plugin-textdomain'),
        'not_found' => __('No OnePagePosts found.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('No OnePagePosts found in Trash.', 'your-plugin-textdomain'),
        'parent' => __('Parent Super Duper'),
    );

    $args = array(
        'label' => __('Article', 'text_domain'),
        'description' => __('Articles', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions','page-attributes','post-formats'),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => false,
        // 'capability_type' => 'page',
    );

    register_post_type('OnePagePost', $args);
}
add_action('init', 'OnePagePost_register');

function genHTMLSelectTag($sLabel, $sSelectName, $asSelectOptions, $sSelectedOption)
{
    // echo '<label>value='.$slanguage.'   </label>';

    $sOutput = '';
    $sOutput = $sOutput.'<label>'.$sLabel.'</label>';
    $sOutput = $sOutput.'<select name="'.$sSelectName.'">';
    foreach ($asSelectOptions as $sSelectOption) {
        $sSelected = '';
        if ($sSelectedOption == $sSelectOption) {
            $sSelected = 'selected';
        }
        $sOutput = $sOutput.'<option '.$sSelected.' value="'.$sSelectOption.'">'.$sSelectOption.'</option>';
    }

    $sOutput = $sOutput.'</select>';

    return $sOutput;
}

function meta_options()
{
    // llaw, multilingal support
    global $post;
    $custPost = get_post_custom($post->ID);

    $slanguage = $custPost['language'][0];
    $sSection_theme = $custPost['section_theme'][0];

    $sHTMLLanguageOption = genHTMLSelectTag('language', 'language', array('en', 'zh'), $slanguage);
    echo $sHTMLLanguageOption;
    echo '<br/>';

    //section themeing support
    //TODO:440 combine variables $asSectionName
    //TODO:80 DISPLAY ONLY IN 1st layer post
    $getSectionName = new cls_section();

    // $asSectionName = array_keys($getSectionName->sSectionTemplate);
    $asSectionName = get_class_methods($getSectionName);

    $sHTMLSectionThemeOption = genHTMLSelectTag('section_theme', 'section_theme', $asSectionName, $sSection_theme);
    echo $sHTMLSectionThemeOption;
    echo '<br/>';

    // add for support in menu text
    // on works on 1st layer post
    // TODO: should only display in 1st layer post

$sMenuText = $custPost['MENU_TEXT'][0];
    $sInputHTML = <<<EOF_SINPUTHTML
<br><label>MENU TEXT</label><br>
<input type="text" name="MENU_TEXT" length="20" value="<!--__MENU_TEXT_VALUE__-->">
<br>
EOF_SINPUTHTML;

    $sInputHTML = preg_replace('/<!--__MENU_TEXT_VALUE__-->/', $sMenuText, $sInputHTML);
    echo $sInputHTML;
}
//Write
function save_cust_post()
{
    global $post;
    update_post_meta($post->ID, 'language', $_POST['language']);
    update_post_meta($post->ID, 'section_theme', $_POST['section_theme']);
    update_post_meta($post->ID, 'MENU_TEXT', $_POST['MENU_TEXT']);
}
add_action('save_post', 'save_cust_post');

function admin_init()
{
    add_meta_box('OnePagePost-meta', 'OnePagePost Options', 'meta_options', 'OnePagePost', 'side', 'low');
}
add_action('admin_init', 'admin_init');
