<?php
    include dirname(__FILE__).'/cust_functions.php';
$sOrigional = <<<EOF_SORIGIONAL
<li class="hidden">
    <a href="#page-top"></a>
</li>
<li>
    <a class="page-scroll" href="#services">Services</a>
</li>
<li>
    <a class="page-scroll" href="#portfolio">Portfolio</a>
</li>
<li>
    <a class="page-scroll" href="#about">About</a>
</li>
<li>
    <a class="page-scroll" href="#team">Team</a>
</li>
<li>
    <a class="page-scroll" href="#contact">Contact</a>
</li>
<li>
    <a class="page-scroll" href="/wp-admin">Admin</a>
</li>
EOF_SORIGIONAL;

    function fGenNavMenu()
    {
        // NOTE: work on the custom option

        // NOTE: input
        // NOTE: process
        // NOTE: output

        # FIXME fix the menu link
        // $sTemplate = '<li><a class="page-scroll" href="<!--__MENU_LINK__-->"><!--__MENU_TEXT__--></a></li>';
        $sTemplate = '<li><a class="page-scroll" href="#<!--__MENU_ANCHOR__-->"><!--__MENU_TEXT__--></a></li>';

        $mainpage_id = get_posts(array(
            'post_parent' => 0,
            'post_type' => 'onepagepost',
            'orderby' => 'menu_order',
            'order' => 'ASC',
        ));

        // mainpage_id should contain 1 post only
        $subpages = get_posts(array(
            'post_parent' => $mainpage_id[0]->ID,
            'post_type' => 'onepagepost',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => 99,
        ));

        foreach ($subpages as $subpage) {
            $sTemp = $sTemplate;
            $custpara = get_post_custom($subpage->ID);

            //MENU_TEXT parameters exist??
            if (array_key_exists('MENU_TEXT', $custpara)) {
                // fHTML_varexport($custpara['MENU_TEXT']);

                if ($custpara['MENU_TEXT'][0] == '_') {
                } elseif ($custpara['MENU_TEXT'][0] == '') {
                    # code...
                } else {
                    // fHTML_varexport($custpara);
                    // fHTML_varexport($subpage);

                    $aReplace = [];
                    array_push($aReplace, array(
                            'key' => 'MENU_TEXT',
                            'value' => $custpara['MENU_TEXT'][0],
                        ));

                        // NOTE: create HTML anchor
                    array_push($aReplace, array(
                            'key' => 'MENU_ANCHOR',
                            'value' => str_replace(' ', '_', $custpara['MENU_TEXT'][0]),
                        ));

                    // fHTML_varexport($aReplace);

                    $shouldRemoveMe = new cls_section();
                    $sTemp = $shouldRemoveMe->fReplaceKeyWords($aReplace, $sTemp);
                    $sOutput = $sOutput."\r\n".$sTemp."\r\n";
                }
            }
        # note close for loop
        }
        // fHTML_varexport($sOutput);
        return $sOutput;
    }
