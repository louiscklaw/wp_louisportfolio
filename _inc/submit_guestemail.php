<?php
  function fHelloworld()
  {
  }

  function fInitMysqliDB()
  {
      global $username;
      global $password;
      global $database;
      global $host;
      global $TBL_USER_GUEST_TOKEN;

      try {
          $db = new MysqliDb($host, $username, $password, $database);
      } catch (Exception $e) {
          print_r($e->getMessage());
      }
  }

  function fDeductCountByGuestCode($sGuestCode)
  {
      global $TBL_USER_GUEST_TOKEN;
      // NOTE: deduct count by guest code
      $bOutput = false;
      try {
          $db = MysqliDb::getInstance();
          $db->where('guestcode', $sGuestCode);
          $iCurrentCount = $db->getOne($TBL_USER_GUEST_TOKEN)['guestcode'];
          if ($iCurrentCount > 0) {
              $db->update($TBL_USER_GUEST_TOKEN, array('iCountdown' => $iCurrentCount - 1));
              $bOutput = true;
          }
      } catch (Exception $e) {
          print_r($e->getMessage());
      }

      return $bOutput;
  }

  function fAddGuestEmail($sEmailToAdd, $sEmailMd5, $sToken)
  {
      global $username;
      global $password;
      global $database;
      global $host;
      global $TBL_USER_GUEST_TOKEN;

      try {
          $db = new MysqliDb($host, $username, $password, $database);
          $data = array(
            'email' => $sEmailToAdd,
            'guestcode' => $sEmailMd5,
            'iCountdown' => $sToken,
            );
          print_r("\r\nTry to add email to table...");
          $db->insert($TBL_USER_GUEST_TOKEN, $data);

          print_r("\r\nAdd email to table done");
          print_r("\r\n");
      } catch (Exception $e) {
          print_r($e->getMessage());
      }
  }

  function fCheckGuestCodeCountdown($sGuestCode)
  {
      try {
          $db = new MysqliDb($host, $username, $password, $database);
          $data = array(
          'guestcode' => $sGuestCode,
        );
      } catch (Exception $e) {
          print_r($e->getMessage());
      }
  }

  require_once 'PHP-MySQLi-Database-Class/MysqliDb.php';

  $username = 'littleredflower';
  $password = 'littleredflower';
  $database = 'littleredflower';
  $host = 'localhost';
  $TBL_USER_GUEST_TOKEN = 'user_guest_token';

  $sDEFAULTCOUNTDOWN = 10;
  $sLinkTemplate = 'http://www.louislabs.com/?guestcode=<GUESTCODE>';

  // print_r('/'.$argv[0]);
  // print_r(__FILE__);
  // print_r(strpos('/'.$argv[0], __FILE__));
  if (isset($argv[0])) {
      if (strpos(__FILE__, '/'.$argv[0])) {
          try {
              print_r("\r\nstart adding email");
              print_r("\r\nEmail to add : ");

              $sEmailToAdd = readline();
              while ($sEmailToAdd == '') {
                  print_r("\r\ninput error !!!");
                  $sEmailToAdd = readline();
              }
              $sMd5SumEmail = md5($sEmailToAdd);

              // NOTE: default countdown
              $sCountdown = $sDEFAULTCOUNTDOWN;
              print_r('Countdown['.$sCountdown.']: ');
              $sTemp = readline();
              $sCountdown = $sTemp;

              print_r("\r\nConfirm         : ");
              print_r("\r\nemail           : $sEmailToAdd ");
              print_r("\r\nmd5             : $sMd5SumEmail");
              print_r("\r\ncountdown       : $sCountdown  ");

              fAddGuestEmail($sEmailToAdd, $sMd5SumEmail, $sCountdown);

              print_r("\r\nAdd complete !!");
              $sLinkGenerate = str_replace('<GUESTCODE>', $sMd5SumEmail, $sLinkTemplate);
              print_r("\r\n");
              print_r("\r\nGenerate        : ");
              print_r("\r\nLink to access  : $sLinkGenerate");
              print_r("\r\nGoodLuck ... ");
              print_r("\r\n");
          } catch (Exception $e) {
              print_r($e->getMessage());
          }
      } else {
      }
  }
