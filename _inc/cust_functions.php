<?php

if (function_exists('fReplaceKeywordsWithLink')) {
    // bypassme??
} else {
    function fReplaceKeywordsWithLink($sContent)
    {
        // input
        $sOutput = $sContent;
        // echo 'fReplaceKeywordsWithLink start';

        // process
        $aKeyPair = fParseWPContentTable($GLOBALS['_wp_explain']->post_content);

        // $sOutput = preg_replace('/123/', 'test_replace_python', $sOutput);

        for ($i = 1;$i <= count($aKeyPair);++$i) {
            $sTarget = '/\['.$aKeyPair[$i]['key'].'\]/';
            // echo 'findme';
            // echo $sOutput;
            // echo 'soutput';
            // echo $sTarget;
            // echo 'starget';
            // die();
            // if (preg_match('$sTarget', $sOutput)) {
            // if (preg_match('/python/', $sOutput)) {

                $sOutput = preg_replace($sTarget, $aKeyPair[$i]['value'], $sOutput);
            // } else {
                // echo 'should not see me';
            // echo  $sOutput;
            // echo preg_match('/python/', $sOutput);
                // echo 'output';
                // fHTML_varexport($sOutput);
                // echo 'target';
                // fHTML_varexport($sTarget);
                // echo 'value';
                // fHTML_varexport($aKeyPair[$i]['value']);
            // }
        }
        // echo 'findme';
        // fHTML_varexport($sOutput);
        // output
        // die();

        return $sOutput;
    }
}

if (function_exists('fFindAndReplaceAnchor')) {
    // bypassme??
} else {
    function fFindAndReplaceAnchor($sContent, $idSectionPost)
    {
        // input
        $sOutput = $sContent;

        // process
        $wp_psot = get_post($idSectionPost);
        $custpara = get_post_custom($idSectionPost);
        // IDEA: if the MENU_TEXT not exist, replace ANCHOR itself
        $sAnchor = str_replace(' ', '_', $custpara['MENU_TEXT'][0]);
        $sOutput = str_replace('<!--__ANCHOR__-->', $sAnchor, $sOutput);

        // output
        return $sOutput;
    }
}

if (function_exists('fConvRFC')) {
    // bypassme?
} else {
    function fConvRFC($sContent)
    {
        // input
        $sOutput = $sContent;
        // process
        $sTemplate = '';
        // NOTE: $1 for matching RFC number in preg
        $sTemplate = '<a href="http://www.rfc-editor.org/info/rfc$1" target="_blank">RFC$1</a>';
        if (preg_match('/RFC\d+/', $sOutput)) {
            $sOutput = preg_replace('/RFC(\d+)/', $sTemplate, $sOutput);
            // echo 'okok';
        } else {
            // echo 'should not see me';
        }
        // output
        return $sOutput;
    }
}

if (function_exists('fConvBadge')) {
} else {
    // include './cust_settings.php';


    function fConvBadge($sContent)
    {
        // global $aaBadgeShortcutConfig;
        $sIMGTag = <<<EOF_SIMGTAG
<img class="shadowed programmer_badge_icon programming_badge_tray_icon_blackshadow" src="IMG_SRC" alt="IMG_ALT" style="margin:3px; max-height:IMG_HEIGHTpx; max-width:IMG_HEIGHTpx" >
EOF_SIMGTAG;

        $asLanguageMark = array(
          'github_icon' => array('github_icon.png'                 , '' , 'https://github.com/about')                                     ,
          'github_text_icon' => array('github_text_icon.svg'       , '' , 'https://github.com/about')                                     ,
          'mysql_icon' => array('mysql_icon.svg'                   , '' , 'https://www.mysql.com/about/')                                 ,
          'mariadb_icon' => array('mariadb_icon.png'               , '' , 'https://mariadb.org/about/')                                   ,
          'postgresql_icon' => array('postgresql_icon.svg'         , '' , 'http://www.postgresql.org/about/')                             ,
          'docker_icon' => array('docker_icon.svg'                 , '' , 'https://www.docker.com/what-docker')                           ,
          'csharp_icon' => array('csharp_icon.png'                 , '' , 'https://msdn.microsoft.com/en-us/library/z1zx9t92.aspx')       ,
          'php_icon' => array('php_icon.svg'                       , '' , 'http://php.net/manual/en/intro-whatis.php')                    ,
          'openshift_icon' => array('openshift_icon.svg'           , '' , 'https://www.openshift.com/')                                   ,
          'ubuntu_icon' => array('ubuntu_icon.svg'                 , '' , 'http://www.ubuntu.com/')                                       ,
          'ubuntu_logo' => array('ubuntu_logo.svg'                 , '' , 'http://www.ubuntu.com/')                                       ,
          'adt_icon' => array('adt_icon.png'                       , '' , 'http://developer.android.com/sdk/index.html')                  ,
          'robotframework_icon' => array('robotframework_icon.png' , '' , 'http://robotframework.org/')                                   ,
          'angularjs_icon' => array('angularjs_icon.svg'           , '' , 'https://docs.angularjs.org/guide/introduction')                ,
          'cubieboard_icon' => array('cubieboard_icon.png'         , '' , 'https://en.wikipedia.org/wiki/Cubieboard')                     ,
          'rpi_icon' => array('rpi_icon.svg'                       , '' , 'https://www.raspberrypi.org/about/')                           ,
          'perl_icon' => array('perl_icon.png'                     , '' , 'https://www.perl.org/')                                        ,
          'html5_icon' => array('html5_icon.svg'                   , '' , 'https://en.wikipedia.org/wiki/HTML5')                          ,
          'vmware_icon' => array('vmware_icon.svg'                 , '' , 'https://en.wikipedia.org/wiki/VMware')                         ,
          'vmware_text_icon' => array('vmware_text_icon.svg'       , '' , 'https://en.wikipedia.org/wiki/VMware')                         ,
          'sourcetree_icon' => array('sourcetree_icon.svg'         , '' , 'https://www.atlassian.com/software/sourcetree/overview/')      ,
          'mongodb_icon' => array('mongodb_icon.svg'               , '' , 'https://www.mongodb.com/company')                              ,
          'ruby_icon' => array('ruby_icon.svg'                     , '' , 'https://www.ruby-lang.org/en/about/')                          ,
          'drupal_icon' => array('drupal_icon.svg'                 , '' , 'https://www.drupal.org/about')                                 ,
          'css3_icon' => array('css3_icon.svg'                     , '' , 'https://en.wikipedia.org/wiki/Cascading_Style_Sheets')         ,
          'fedora_icon' => array('fedora_icon.svg'                 , '' , 'https://getfedora.org/')                                       ,
          'fedora_text_icon' => array('fedora_text_icon.svg'       , '' , 'https://getfedora.org/')                                       ,
          'nodejs_icon' => array('nodejs_icon.svg'                 , '' , 'https://nodejs.org/en/about/')                                 ,
          'jenkins_icon' => array('jenkins_icon.svg'               , '' , 'https://jenkins-ci.org/content/about-jenkins-ci/')             ,
          'virtualbox_icon' => array('virtualbox_icon.png'         , '' , '')                                                             ,
          'arduino_icon' => array('arduino_icon.svg'               , '' , 'https://www.arduino.cc/en/Guide/Introduction')                 ,
          'jquery_icon' => array('jquery_icon.svg'                 , '' , 'https://learn.jquery.com/about-jquery/')                       ,
          'wordpress_icon' => array('wordpress_icon.svg'           , '' , 'https://wordpress.com/')                                       ,
          'bootstrap_icon' => array('bootstrap_icon.svg'           , '' , 'http://getbootstrap.com/about/')                               ,
          'apache_icon' => array('apache_icon.png'                 , '' , 'https://en.wikipedia.org/wiki/Apache_HTTP_Server')             ,
          'python_icon' => array('python_icon.svg'                 , '' , 'https://www.python.org/')                                      ,
          'atom_icon' => array('atom_icon.svg'                     , '' , 'https://atom.io/')                                             ,
          'android_icon' => array('android_icon.svg'               , '' , 'https://en.wikipedia.org/wiki/Android_%28operating_system%29') ,
          'bitbucket_icon' => array('bitbucket_icon.png'           , '' , 'https://bitbucket.org/')                                       ,
          'bitbucket_text_icon' => array('bitbucket_text_icon.png' , '' , 'https://bitbucket.org/')                                       ,
          'ga_icon' => array('ga_icon.png'                         , '' , 'http://www.google.com/analytics/')                             ,
          'google_maps_icon' => array('google_maps_icon.svg'       , '' , 'http://www.google.com/analytics/')                             ,
          'wireshark_icon' => array('wireshark_icon.png'           , '' , 'http://www.google.com/analytics/')                             ,
          'apache2_text_icon' => array('apache2_text_icon.png'     , '' , 'http://www.google.com/analytics/')                             ,
          'typekit_icon' => array('typekit_icon.png'               , '' , 'http://www.google.com/analytics/')                             ,
          'fiddler_icon' => array('fiddler_icon.svg'               , '' , 'http://www.google.com/analytics/')                             ,
          'windows_icon' => array('windows_icon.svg'               , '' , '')                                                             ,
          'labview_icon' => array('labview_icon.png'               , '' , '')                                                             ,
          'youtube_icon' => array('youtube_icon.png'               , '' , '')                                                             ,
          'magento_icon' => array('magento_icon.png'               , '' , '')                                                             ,
          'codeignitor_icon' => array('codeignitor_icon.svg'       , '' , '')                                                             ,
          'laravel_icon' => array('laravel_icon.png'               , '' , '')                                                             ,
          'youtube_icon' => array('youtube_icon.png'               , '' , '')                                                             ,
          'twitter_icon' => array('twitter_icon.png'               , '' , '')                                                             ,
          'facebook_icon' => array('facebook_icon.png'             , '' , '')                                                             ,
          );

        $sOutput = $sContent;

        $stest = 'test';
        // $sBaseIMGDir=get_home_url().'/wp-content/plugins/programmers_badge/_img';
        # NOTE redirect from plugin directory to theme directory
        $sBaseIMGDir = get_template_directory_uri().'/img/programmers_badge';

        //  $sOutput = '123';
        foreach ($asLanguageMark as $sLanguageMark => $array) {
            //   fHTML_varexport($array[1]);
            $sIconFile = $array[0];
            $sAbout = $array[1];    // NOTE: will be the alternative text for icon
            $sHref = $array[2];        // TODO: add <a href> for icon
            $aaBadgeShortcutConfig[$sLanguageMark]['src'] = $sBaseIMGDir.'/'.$sIconFile;
            $aaBadgeShortcutConfig[$sLanguageMark]['alt'] = ($sAbout == '' ? $sLanguageMark : $sAbout);
            $aaBadgeShortcutConfig[$sLanguageMark]['href'] = ($sHref == '' ? 'https://en.wikipedia.org/wiki/'.explode('_', $sIconFile)[0] : $sHref);  // NOTE: how to tell ??
        }

        // NOTE: test usage
        $sTEST_TEXT = 'test_all';
        $sTestPattern = '/\[('.$sTEST_TEXT.'):(\d+)\]/';
        // if (false) {
        if (preg_match($sTestPattern, $sOutput)) {
            // NOTE: input
            preg_match_all($sTestPattern, $sOutput, $sParameters);
            // NOTE input  => [test_all:50]
            // NOTE output => array (
            //   0 =>
            //   array (
            //     0 => '[test_all:50]',
            //   ),
            //   1 =>
            //   array (
            //     0 => 'test_all',
            //   ),
            //   2 =>
            //   array (
            //     0 => '50',
            //   ),
            // )

            for ($i = 0; $i < count($sParameters); ++$i) {
                $IMG_HEIGHT = $sParameters[2][$i];
                $sTemp = '';

                foreach ($aaBadgeShortcutConfig as $sKey => $array) {
                    $sIMG_SRC = $array['src'];
                    $sIMG_ALT = $array['alt'];
                    $sHref = $array['href'];

                    // NOTE: generate target string
                    $sGenerateString = $sIMGTag;
                    $sGenerateString = preg_replace('/'.'IMG_SRC'.'/', $sIMG_SRC, $sGenerateString);
                    $sGenerateString = preg_replace('/'.'IMG_HEIGHT'.'/', $IMG_HEIGHT, $sGenerateString);
                    $sGenerateString = preg_replace('/'.'IMG_ALT'.'/', $sIMG_ALT, $sGenerateString);

                    $sTemp = $sTemp.$sGenerateString;
                }
                $sOutput = preg_replace('/\[('.$sTEST_TEXT.'):('.$IMG_HEIGHT.')\]/', $sTemp, $sOutput);
            }
        }
        foreach ($aaBadgeShortcutConfig as $sKey => $array) {
            // NOTE: normal usage
            $sToBeReplaced = '/\[('.$sKey.'):(\d+)\]/';

            if (preg_match($sToBeReplaced, $sOutput)) {
                // NOTE: string in content needs to be replaced
                // NOTE: default for IMG_ALT
                preg_match_all($sToBeReplaced, $sOutput, $sParameters);

                $IMG_HEIGHT = '5'; // NOTE: default if not match
                for ($i = 0; $i < count($sParameters[2]); ++$i) {
                    $IMG_HEIGHT = $sParameters[2][$i];

                    $sIMG_SRC = $array['src'];
                    $sIMG_ALT = $array['alt'];
                    // $IMG_HEIGHT = $sParameters[2][0];
                    $sHref = $array['href'];

                    // NOTE: generate target string
                    $sGenerateString = $sIMGTag;
                    $sGenerateString = preg_replace('/'.'IMG_SRC'.'/', $sIMG_SRC, $sGenerateString);
                    $sGenerateString = preg_replace('/'.'IMG_HEIGHT'.'/', $IMG_HEIGHT, $sGenerateString);
                    $sGenerateString = preg_replace('/'.'IMG_ALT'.'/', $sIMG_ALT, $sGenerateString);

                    $sGenerateString = ($sHref == '' ? $sGenerateString : '<a href="'.$sHref.'">'.$sGenerateString.'</a>');

                    $sOutput = preg_replace('/\[('.$sKey.'):('.$IMG_HEIGHT.')\]/', $sGenerateString, $sOutput);
                }
            } else {
                // NOTE: haha , why i am here ?
            }
        // fHTML_varexport($sOutput);
        }

        return $sOutput;
    // NOTE: close function
    }
// NOTE: close if
}

if (function_exists('fParseWPContentTable')) {
} else {
    function fParseWPContentTable($strHTML)
    {
        $aOutput = array();

        // TODO:1090 test todo

        $DOM = new DOMDocument();
        $DOMKeyNames = new DOMDocument();
        $DOMValues = new DOMDocument();

        $DOM->loadHTML('<?xml encoding="utf-8" ?>'.$strHTML);
        $All_tr = $DOM->getElementsByTagName('tr');
        // error_log(var_dump($All_tr->item(0)));
        // die();

        // slice 1st row html for keyname
        // $keyNamesHTML = $DOM->saveHTML($All_tr[0]);
        $keyNamesHTML = $DOM->saveHTML($All_tr->item(0));
        $DOMKeyNames->loadHTML('<?xml encoding="utf-8" ?>'.$keyNamesHTML);
        $keyNames = $DOMKeyNames->getElementsByTagName('td');

        for ($i = 1;$i < $All_tr->length;++$i) {
            $DOMValues->loadHTML('<?xml encoding="utf-8" ?>'.$DOM->saveHTML($All_tr->item($i)));
            $all_td = $DOMValues->getElementsByTagName('td');

            for ($k = 0;$k < $keyNames->length;++$k) {
                // if ($keyNames[$k]->textContent == '') {
                if ($keyNames->item($k)->textContent == '') {
                } else {
                    //var_export($keyNames[$k]->textContent);
                    // $sKeyNames = $keyNames[$k]->textContent;
                    $sKeyNames = $keyNames->item($k)->textContent;
                    $sValues = $DOMValues->saveHTML($all_td->item($k));
                    $sValues = preg_replace('/^<td>/', '', $sValues);
                    $sValues = preg_replace('/<\/td>$/', '', $sValues);

                    $aOutput[$i][$sKeyNames] = $sValues;
                }
            }
        }

        return $aOutput;
    // NOTE: close function
    }
// NOTE: close if
}

if (function_exists('fCheckHTMLElements')) {
} else {
    function fCheckHTMLElements($strHTML, $sHTMLElement)
    {
        $bOutput = false;

        $DOM = new DOMDocument();

        $sTest = $DOM->loadHTML($strHTML);
        $checkTables = $DOM->getElementsByTagName($sHTMLElement);

        if ($checkTables->length > 0) {
            //   error_log('start');
        //   error_log($strHTML);
        //   error_log(count($checkTables));
          $bOutput = true;
        //   error_log('end');
        }

    //   error_log($strHTML);
    //   error_log($bOutput);

      return $bOutput;
  // NOTE: close function
    }
// NOTE: close if
}

if (function_exists('fHTML_varexport')) {
} else {
    function fHTML_varexport($sInput)
    {
        // $sHTMLLog='';

        if ($_SERVER['ServerType'] == 'debug') {
            echo '<pre class="should_not_see_me debug_php_log" style="visibility:hidden">'."\r\n";
            var_export($sInput);
            echo "\r\n</pre>\r\n";
        }
        // return "\r\n".$sHTMLLog."\r\n";
    // NOTE: close function
    }
// NOTE: close if
}

if (function_exists('f_DB_error_log')) {
} else {
    function f_DB_error_log($sErrorInput, $sType = 'info')
    {
        // TODO: release me
        // require_once 'PHP-MySQLi-Database-Class/MysqliDb.php';
        // $db = new MysqliDb('localhost', $GLOBALS['DB_USER'], $GLOBALS['DB_PASS'], $GLOBALS['DB_NAME']);
        // $db->setPrefix('user_');
        // $id = $db->insert('errorlog',
        //     array(
        //         'type' => strtoupper($sType),
        //         'message' => $sErrorInput,
        //     )
        // );
    }
}

if (function_exists('sanitize_output')) {
} else {
    function sanitize_output($input)
    {
        $buffer = $input;

        // TODO: remvoe html, js comment first


        // TODO: better manage of pairs
        $search = array(
            '/i_am_origional_testvector/s',  // test vector
            '/\>[^\S ]+/s', //strip whitespaces after tags, except space
            '/[^\S ]+\</s', //strip whitespaces before tags, except space
            '/(\s)+/s',  // shorten multiple whitespace sequences
            '/(\r)+/s',  // shorten multiple whitespace sequences
            '/(\n)+/s',  // shorten multiple whitespace sequences
            '/(\r\n)+/s',  // shorten multiple whitespace sequences
            '/(\> \<)+/s',  // shorten multiple whitespace sequences
            );
        $replace = array(
            'i_am_target_testvector',
            '>',
            '<',
            '\\1',
            '',
            '',
            '',
            '><',
            );
        $buffer = preg_replace($search, $replace, $buffer);

        return $buffer;
    }
}
