<?php
/*
 * Template Name: index.php
 */
?>
<?php
  mb_internal_encoding('utf-8');

  // include dirname(__FILE__).'/config.php';
  // include get_template_directory_uri().'/config.php';
  include get_template_directory().'/config.php';

  ob_start('sanitize_output');
  include $GLOBALS['DIR_INC'].'/http_request_log.php';
  //include dirname(__FILE__).'/../cust_functions.php';
  include $GLOBALS['DIR_INC'].'/cust_functions.php';

  // TODO: remove comment
  // fLogServerVar($_SERVER);
  f_DB_error_log('index.php start');
?>
<?php
$sHTML_start = <<<EOF_HTML_START
<!DOCTYPE html>
<html lang="en">
EOF_HTML_START;
echo $sHTML_start;
?>
  <?php
    #input
    $mainpost = get_posts(
      array(
        'post_type' => 'onepagepost',
        'post_parent' => 0,
      )
    );

    $all_posts = get_posts(
      array(
        'post_type' => 'onepagepost',
        'posts_per_page' => 999,
      )
    );
    $GLOBALS['allposts'] = $all_posts;

    // NOTE: common system post
    for ($i = 0; $i < count($all_posts);++$i) {
        if (substr($all_posts[$i]->post_title, 0, 4) == '_wp_') {
            $GLOBALS[$all_posts[$i]->post_title] = $all_posts[$i];
        } else {
            // $GLOBALS['_explain'] = $all_posts[$i];
        }
    }

    // echo 'findme';
    // fHTML_varexport($GLOBALS['_wp_explain']->post_content);

    #process
    # TODO enhance multilingual support
    #when show mainpage:
    #when rendering page:
    #default lang flag => en
    #if lang flag is not set:
        #check lang options according to source IP(geoIP)
        #set lang flag

    $lang = 'en';
    if (isset($_GET['flag_lang']) && $_GET['flag_lang']) {
        $lang = $_GET['flag_lang'];
    }

    # NOTE enhance page maintaince support
    #if enabled == 1 in mainpage:
        # show mainpage
    # if enabled == 0 in mainpage:
        #if bypass flag not found:
            # go 404 page/go waiting page
        #if bypass flah found:
            # show mainpage

     $aTest = fParseWPContentTable($mainpost[0]->post_content);
     error_log('haa');

     // TODO: should i generalize this ??? im _waiting_page
    if ($aTest[fGetTargetKeyFromArray('_waiting_page', $aTest)]['value'] == '1' && !isset($_GET['bypass_waitingpage'])) {
        include $GLOBALS['DIR_INC'].'/waiting_page/index.php';
    } else {
        include 'index_mainpage.php';
    }

  #output
$sHTML_end = <<<EOF_HTML_END
</html>
EOF_HTML_END;
echo $sHTML_end;
ob_flush();
   ?>
