<?php
  include 'config.php';

  // use for register custom post type
  include $GLOBALS['DIR_INC'].'/cpt_onepagepost.php';
  add_theme_support('post-thumbnails');

  // fParseWPContentTable move to cust_functions.php
  // fCheckHTMLElements move to cust_functions.php


  // add support for non-standard file type upload
  add_filter('upload_mimes', 't4a_add_custom_upload_mimes');

  function t4a_add_custom_upload_mimes($existing_mimes){
      return array_merge($existing_mimes, array(
          'csv' => 'application/octet-stream',
          'xml' => 'application/atom+xml',
          '7z' => 'application/x-7z-compressed',
          'rar' => 'package/rar',
          'tar' => 'package/x-tar',
          'tgz' => 'application/x-tar-gz',
          'apk' => 'application/vnd.android.package-archive',
          'zip' => 'package/zip',
          'img|iso' => 'package/img',
          'gz|gzip' => 'package/x-gzip',
          'deb|rpm' => 'package/x-app',
          'ttf|woff' => 'application/x-font') );
      return $existing_mimes;
  }

  function show_language_column_head($defaults)
  {

      if ($_SERVER['ServerType'] == 'debug') {
          $defaults['debug_postinfo'] = 'debug_postinfo';
      }
      $defaults['language'] = 'language';

      // $defaults['sSelectionBox'] = 'sSelectionBox';

      $defaults['Parent'] = 'Parent';
      $defaults['section_theme'] = 'section_theme';

      return $defaults;
  }
  add_filter('manage_onepagepost_posts_columns', 'show_language_column_head');

   function show_language_column($column_name, $post_ID)
   {
       $post_info = get_post($post->ID);
       $custom_info = get_post_custom($post->ID);

       if ($column_name == 'language') {
           //  echo var_dump($custom_info['language'][0]);
          echo $custom_info['language'][0];
          // var_dump($custom_info);
          // echo "haha";
       }else if ($column_name == 'section_theme') {
           //  echo var_dump($custom_info['language'][0]);
          echo $custom_info['section_theme'][0];
          // var_dump($custom_info);
          // echo "haha";
       }

       if ($column_name == 'Parent') {
           //  echo var_dump($custom_info['language'][0]);
          echo get_post($post_info->post_parent)->post_title;
          // var_dump($custom_info);
          // echo "haha";
       }

       if ($_SERVER['ServerType'] == 'debug') {
           if ($column_name == 'debug_postinfo') {
              //  echo var_dump($custom_info['language'][0]);
              // var_export($post_info);
              // var_dump($custom_info);
              // echo "haha";
           }
       }
   }
  // add_action('manage_posts_custom_column', 'show_language_column', 10, 2);
  add_action('manage_onepagepost_posts_custom_column', 'show_language_column', 10, 2);

  // add for menu support
  function register_my_menu()
  {
      register_nav_menu('header-menu', __('Header Menu'));
  }
  add_action('init', 'register_my_menu');

  // for SVG upload
  function cc_mime_types($mimes)
  {
      $mimes['svg'] = 'image/svg+xml';

      return $mimes;
  }
add_filter('upload_mimes', 'cc_mime_types');
