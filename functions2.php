<?php

  define('MY_POST_TYPE', 'my');
  define('MY_POST_SLUG', 'gallery');

    function my_meta_box_details()
    {
        global $post;
        $post_ID = $post->ID; // global used by get_upload_iframe_src
      printf("<iframe frameborder='0' src=' %s ' style='width: 100%%; height: 400px;'> </iframe>", get_upload_iframe_src('media'));
    }

  function my_meta_box_cb()
  {
      add_meta_box('test_post_type'.'_details', 'Media Library', 'my_meta_box_details', 'test_post_type', 'normal', 'high');
  }

  function my_register_post_type()
  {
      $args = array(
        'label' => 'Gallery',
        'supports' => array('title', 'excerpt'),
        'register_meta_box_cb' => 'my_meta_box_cb',
        'show_ui' => true,
        'query_var' => true,
    );
      register_post_type('test_post_type', $args);
  }
  add_action('init', 'my_register_post_type');
