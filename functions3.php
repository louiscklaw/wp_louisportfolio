<?php

    $GLOBALS['URI_JS'] = get_template_directory_uri().'/js';
    $GLOBALS['URI_CSS'] = get_template_directory_uri().'/css';
    $GLOBALS['HTTP_IMG_PATH'] = get_template_directory_uri().'/img';
    #$GLOBALS['DIR_INC'] = get_template_directory_uri().'/_inc';

    $GLOBALS['DIR_INC'] = get_template_directory().'/_inc';

    $GLOBALS['PATH']['ROOT']['DIR'] = get_template_directory().'/';
    $GLOBALS['PATH']['ROOT']['URI'] = get_template_directory_uri().'/';

function product_register()
{
    $labels = array(
        'name' => _x('Books', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Book', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Books', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Book', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Add New', 'book', 'your-plugin-textdomain'),
        'add_new_item' => __('Add New Book', 'your-plugin-textdomain'),
        'new_item' => __('New Book', 'your-plugin-textdomain'),
        'edit_item' => __('Edit Book', 'your-plugin-textdomain'),
        'view_item' => __('View Book', 'your-plugin-textdomain'),
        'all_items' => __('All Books', 'your-plugin-textdomain'),
        'search_items' => __('Search Books', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Books:', 'your-plugin-textdomain'),
        'not_found' => __('No books found.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('No books found in Trash.', 'your-plugin-textdomain'),
        'parent' => __('Parent Super Duper'),
    );

    $args = array(
        'label' => __('Article', 'text_domain'),
        'description' => __('Articles', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions','page-attributes'),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => false,
        'capability_type' => 'page',
    );

    register_post_type('product', $args);
}
add_action('init', 'product_register');

//Read
function meta_options()
{
    global $post;

    //test label
    $sLabel = get_post_custom($post->ID);
    $price = $sLabel['price'][0];
    echo '<label>Price:</label><input name="price" value="'.$price.'" />';

    //test selection box
    $sSelectionBox = get_post_custom($post->ID);
    $price = $sSelectionBox['price'][0];
    $sTemp = <<<EOF_STEMP

<br><label>Input type: selection box</label><br>
<select>
 <option value="selection1">selection 1</option>
 <option value="selection2">selection 2</option>
 <option value="selection3">selection 3</option>
 <option value="selection4">selection 4</option>
</select>


<br><label>Input type: number</label><br>
<input type="number" name="quantity" min="1" max="5">
<br>

<br><label>Input type: color</label><br>
<input type="color" name="favcolor">
<br>

<br><label>Input type: checkbox</label><br>
<input type="checkbox" name="vehicle1" value="Bike"> I have a bike
<br>

<br><label>email:</label><br>
<input type="email" name="usremail">
<br>


<br><label>file:</label><br>
<input type="file" name="img">
<br>


<br><label>Input type: number:</label><br>
<input type="number" name="quantity" min="1" max="5">
<br>


<br><label>Input type: radio:</label><br>
<input type="radio" name="gender" value="male"> Male
<input type="radio" name="gender" value="female"> Female
<br>


<br><label>Input type: range:</label><br>
 <input type="range" name="points" min="0" max="10">
<br>



<br><label>Input type: uploader:</label><br>
<a href="#" class="button insert-media add_media" data-editor="content" title="Add Media">
    <span class="wp-media-buttons-icon"></span> Add Media
</a>
<br>









EOF_STEMP;

    echo $sTemp;
}

//Write
function save_price()
{
    global $post;
    update_post_meta($post->ID, 'price', $_POST['price']);

    update_post_meta($post->ID, 'sSelectionBox', $_POST['sSelectionBox']);
}
add_action('save_post', 'save_price');

function admin_init()
{
    add_meta_box('prodInfo-meta', 'Product Options', 'meta_options', 'product', 'side', 'low');
}
add_action('admin_init', 'admin_init');

function show_price_column_head($defaults)
{
    $defaults['price'] = 'Price';
    $defaults['sSelectionBox'] = 'sSelectionBox';

    return $defaults;
}
add_filter('manage_posts_columns', 'show_price_column_head');

 function show_price_column($column_name, $post_ID)
 {
     $custom = get_post_custom($post->ID);
     if ($column_name == 'price') {
         //  echo var_dump($custom['price'][0]);
         echo $custom['price'][0];
     }
     if ($column_name == 'sSelectionBox') {
         //  echo var_dump($custom['price'][0]);
         echo var_dump($custom['sSelectionBox']);
     }
 }
add_action('manage_posts_custom_column', 'show_price_column', 10, 2);

register_taxonomy('catalog', array('product'), array('hierarchical' => true, 'label' => 'Catalogs', 'singular_label' => 'Catalog', 'rewrite' => true));

function my_meta_box_details()
{
    global $post;
    $post_ID = $post->ID; // global used by get_upload_iframe_src
  printf("<iframe frameborder='0' src=' %s ' style='width: 100%%; height: 400px;'> </iframe>", get_upload_iframe_src('media'));
}

function my_meta_box_cb()
{
    add_meta_box('test_post_type'.'_details', 'Media Library', 'my_meta_box_details', 'test_post_type', 'normal', 'high');
}
